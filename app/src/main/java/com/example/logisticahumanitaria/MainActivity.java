package com.example.logisticahumanitaria;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.backup.SharedPreferencesBackupHelper;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import database.DbEstados;
import database.Device;
import database.Estado;
import database.ProcesosPHP;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, Response.Listener<JSONObject>, Response.ErrorListener {

    private EditText edtNombre;
    private Estado savedEstado;
    private int id;
    private Button btnGuardar;
    private Button btnListar;
    private Button btnLimpiar;
    private Button btnSalir;
    private boolean estadosAgregados;
    private ArrayList<Estado> listaEstados;
    private ProcesosPHP php;

    //Sincronizacion de datos
    private final Context context = this;
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
        setEvents();

        //Sincronizacion
        if(isNetworkAvailable()) {
            request = Volley.newRequestQueue(this);
            consultarTodoWebService();
        }
    }

    public void initComponents() {
        this.php = new ProcesosPHP();
        php.setContext(this);
        btnGuardar = findViewById(R.id.btnGuardar);
        btnListar = findViewById(R.id.btnListar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        edtNombre = findViewById(R.id.txtNombre);
        btnSalir = findViewById(R.id.btnSalir);
        savedEstado = null;
    }

//Botones de salir y limpiar
    public void setEvents() {
        this.btnGuardar.setOnClickListener(this);
        this.btnListar.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
        this.btnSalir.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        if (isNetworkAvailable()){

            switch (view.getId()) {
                case R.id.btnGuardar:

                    if (edtNombre.getText().toString().equals("")) {
                        edtNombre.setError("Introduce el Estado");
                    }else {
                        DbEstados source = new DbEstados(MainActivity.this);
                        source.openDatabase();
                        Estado estados = new Estado();
                        estados.setNombre(edtNombre.getText().toString());
                        if (savedEstado == null) {
                            source.insertEstado(estados);
                            estados.setIdMovil(Device.getSecureId(this));
                            php.insertarEstadoWebService(estados);
                            Toast.makeText(MainActivity.this, R.string.agregado, Toast.LENGTH_SHORT).show();
                            limpiar();
                        } else {
                            source.updateEstado(estados, id);
                            Toast.makeText(MainActivity.this,"id= "+ id, Toast.LENGTH_SHORT).show();
                            estados.setIdMovil(Device.getSecureId(this));
                            php.actualizarEstadoWebService(estados, id);
                            //Toast.makeText(MainActivity.this,"id= "+ id, Toast.LENGTH_SHORT).show();

                            Toast.makeText(MainActivity.this, R.string.mensajeedit, Toast.LENGTH_SHORT).show();
                            limpiar();
                        }
                        source.close();
                    }
                    break;

                case R.id.btnListar:
                    Intent i = new Intent(MainActivity.this, ListaActivity.class);
                    limpiar();
                    startActivityForResult(i, 0);
                    break;

                case R.id.btnLimpiar:
                    limpiar();
                    break;

                case R.id.btnSalir:
                    finish();
                    break;
            }
        }else if(view.getId() == R.id.btnListar){
            Intent i = new Intent(MainActivity.this, ListaActivity.class);
            limpiar();
            startActivityForResult(i, 0);

        }else if(view.getId() == R.id.btnLimpiar){
            limpiar();
        }else if(view.getId() == R.id.btnSalir){
            finish();
        }else{
            Toast.makeText(getApplicationContext(), "Se necesita tener conexión a internet",Toast.LENGTH_SHORT).show();
        }
    }

    //Botones de limpiar
    public void limpiar() {
        savedEstado = null;
        edtNombre.setText("");
    }

    protected void onActivityResult ( int requestCode, int resultCode, Intent data)
    {
        if (Activity.RESULT_OK == resultCode) {
            Estado estado = (Estado) data.getSerializableExtra("estados");
            savedEstado = estado;
            id = estado.get_ID();
            edtNombre.setText(estado.getNombre());
        } else {
            limpiar();
        }
    }


    public void consultarTodoWebService() {
        String url = "https://logisticahumanitaria.000webhostapp.com/WebService/swConsultarTodo.php";
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.i("ERROR->", error.toString());

    }

    @Override
    public void onResponse(JSONObject response) {
        Estado estado = null;
        JSONArray json = response.optJSONArray("estados");
        DbEstados estadoDB = new DbEstados(this);
        estadoDB.openDatabase();

        try {
            for(int i=0;i<json.length();i++){
                estado = new Estado();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                estado.setNombre(jsonObject.optString("nombre"));
                estadoDB.insertEstado(estado);
            }
            estadoDB.close();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public boolean isNetworkAvailable () {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }

        /*public void insertarEstadosInicial () {
            SharedPreferences preferencias = getSharedPreferences("datos", Context.MODE_PRIVATE);
            this.estadosAgregados = preferencias.getBoolean("estadosAgregados", false);
            if (!this.estadosAgregados) {
                DbEstados source = new DbEstados(MainActivity.this);
                source.openDatabase();
                if (source.allEstado().size() == 0) {
                    String[] diezEstados = {"Sinaloa", "Michoacan", "Sonora", "Chihuahua", "Oaxaca", "San Luis Potosi", "Nuevo Leon", "Puebla", "Veracruz", "Baja California"};
                    for (int x = 0; x < 10; x++) {
                        Estado nuevoEstado = new Estado();
                        nuevoEstado.setNombre(diezEstados[x]);
                        source.insertEstado(nuevoEstado);
                    }
                }
                source.close();
                SharedPreferences.Editor editarPreferencias = preferencias.edit();
                editarPreferencias.putBoolean("estadosAgregados", true);
                editarPreferencias.commit();
            }
        }*/



}

