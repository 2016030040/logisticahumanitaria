package com.example.logisticahumanitaria;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AlertDialogLayout;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import database.DbEstados;
import database.Device;
import database.Estado;
import database.ProcesosPHP;

public class ListaActivity extends ListActivity implements Response.Listener<JSONObject>, Response.ErrorListener {
    private DbEstados dbestado;
    private Button btnNuevo;
    private final Context context = this;
    private ProcesosPHP php = new ProcesosPHP();
    private ArrayList<Estado> listaEstados;
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        /*listaEstados = new ArrayList<Estado>();
        request = Volley.newRequestQueue(context);
        consultarTodoWebService();*/

        //SI TIENE CONEXION -> SINCRONIZA
        if(isNetworkAvailable()){
            listaEstados = new ArrayList<Estado>();
            request = Volley.newRequestQueue(context);
            consultarTodoWebService();
        }else{
            dbestado = new DbEstados(this);
            dbestado.openDatabase();
            ArrayList<Estado> estados = dbestado.allEstado();
            MyArrayAdapter adapter = new MyArrayAdapter(this, R.layout.activity_estado, estados);
            setListAdapter(adapter);
        }


        btnNuevo = ( Button ) findViewById(R.id.btnNuevo);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
    }

    public void consultarTodoWebService() {
        String url = "https://logisticahumanitaria.000webhostapp.com/WebService/swConsultarTodo.php";
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }
    public void borrarEstadoWebService(int id) {
        String url = "https://logisticahumanitaria.000webhostapp.com/WebService/swBorrar.php?id=" + id;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.i("ERROR->", error.toString());

    }

    @Override
    public void onResponse(JSONObject response) {
        Log.i("RESPUESTA->", response.toString());
        Estado estado = null;
        JSONArray json = response.optJSONArray("estados");
        try {
            for(int i=0;i<json.length();i++){
                estado = new Estado();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                estado.set_ID(jsonObject.optInt("_ID"));
                estado.setNombre(jsonObject.optString("nombre"));
                estado.setIdMovil(jsonObject.optString("idMovil"));
                listaEstados.add(estado);
            }
            MyArrayAdapter adapter = new MyArrayAdapter(context,R.layout.activity_estado,listaEstados);
            setListAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    class MyArrayAdapter extends ArrayAdapter<Estado> {
        Context context;
        int textViewResourceId;
        ArrayList<Estado> objects;

        public MyArrayAdapter(Context context, int textViewResourceId, ArrayList<Estado> objects) {
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewResourceId = textViewResourceId;
            this.objects = objects;
        }

        public View getView(final int position, View convertView, ViewGroup
                viewGroup) {
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, null);
            TextView lblNombre = (TextView) view.findViewById(R.id.lblNombreContacto);

            Button modificar = (Button) view.findViewById(R.id.btnModificar);
            Button borrar = (Button) view.findViewById(R.id.btnBorrar);

            lblNombre.setText("Estado: " + objects.get(position).getNombre());


            borrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (isNetworkAvailable()){
                        borrarEstadoWebService(objects.get(position).get_ID());

                        dbestado.openDatabase();
                        dbestado.deleteEstado(objects.get(position).get_ID());
                        dbestado.close();
                        objects.remove(position);
                        notifyDataSetChanged();
                    }else {
                        Toast.makeText(getApplicationContext(), "Se necesita tener conexion a internet",Toast.LENGTH_SHORT).show();
                    }

                }
            });
            modificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("estados", objects.get(position));

                    Intent i = new Intent();
                    i.putExtras(oBundle);
                    setResult(Activity.RESULT_OK, i);
                    finish();
                }
            });
            return view;
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }

}