package database;

import java.io.Serializable;

public class Estado implements Serializable {
    private int _ID;
    private String nombre;
    private String idMovil;

    public Estado() {
        this._ID = 0;
        this.nombre = "";
        this.idMovil="";
    }

    public Estado(int _ID, String nombre,int idMovil) {
        this._ID = _ID;
        this.nombre = nombre;
    }

    public int get_ID() {
        return _ID;
    }

    public void set_ID(int _ID) {
        this._ID = _ID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getIdMovil() {
        return idMovil;
    }

    public void setIdMovil(String idMovil) {
        this.idMovil = idMovil;
    }
}
