package database;

import android.content.Context;
import android.util.Log;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONObject;

import java.util.ArrayList;

public class ProcesosPHP implements Response.Listener<JSONObject>, Response.ErrorListener {
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private ArrayList<Estado> estados = new ArrayList<Estado>();
    private String serverip = "https://logisticahumanitaria.000webhostapp.com/WebService/";

    public void setContext(Context context) {
        request = Volley.newRequestQueue(context);
    }

    public void insertarEstadoWebService(Estado e) {
        String url = serverip + "swAgregar.php?nombre=" + e.getNombre()
                + "&idMovil=" + e.getIdMovil();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public void actualizarEstadoWebService(Estado e, int id) {
        String url = serverip + "swModificar.php?id=" + id + "&nombre=" + e.getNombre() + "&idMovil=" + e.getIdMovil();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public void borrarEstadoWebService(int id) {
        String url = serverip + "swEliminar.php?id=" + id;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public void consultarTodoWebService() {
        String url = serverip + "swConsultarTodo.php";
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public void consultarUnoWebService(String nombre) {
        String url = serverip + "swConsultar.php?" + "&nombre=" + nombre;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.i("ERROR", error.toString());
    }

    @Override
    public void onResponse(JSONObject response) {
    }
}
